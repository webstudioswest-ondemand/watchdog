jQuery(document).ready(function(){
    // add save button to all graphs
    
    jQuery('.visualizer-front').after('<button class="saveSvg fusion-button" href="#">Save as Image</button>');

    // save svg as image
    jQuery(".saveSvg").on("click", function(event) {
        // event.preventDefault();

        // get svg code
        var svghtml = jQuery(this).prev().find('svg')[0];
        
        jQuery(svghtml).attr("version", 1.1).attr("xmlns", "http://www.w3.org/2000/svg");

        // create memory image
        var imgsrc = 'data:image/svg+xml;base64,' + btoa(unescape(encodeURIComponent(svghtml.outerHTML)));
        var img = '<img src="' + imgsrc + '">';
        
        // prepare canvas
        var width = jQuery(this).prev().width();
        var height = jQuery(this).prev().height();
        jQuery(this).after('<canvas class="svgcanvas" width="'+width+'" height="'+height+'" style="display:none"></canvas>');
        
        var canvas = jQuery(this).next()[0];
        var context = canvas.getContext("2d");

        var image = new Image;
        image.src = imgsrc;
        image.onload = function() {
            context.drawImage(image, 0, 0);

            var canvasdata = canvas.toDataURL("image/png");

            var pngimg = '<img src="' + canvasdata + '">';
            // jQuery("#pngdataurl").html(pngimg);

            var a = document.createElement("a");
            a.download = "graph.png";
            a.href = canvasdata;
            a.click();
        };
    });
});
