<?php

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>
<div id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
	<?php while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php echo avada_render_rich_snippets_for_pages(); ?>
			<?php if ( ! post_password_required( $post->ID ) ) : ?>
				<?php if ( Avada()->settings->get( 'featured_images_pages' ) ) : ?>
					<?php if ( 0 < avada_number_of_featured_images() || get_post_meta( $post->ID, 'pyre_video', true ) ) : ?>
						<div class="fusion-flexslider flexslider post-slideshow">
							<ul class="slides">
								<?php if ( get_post_meta( $post->ID, 'pyre_video', true ) ) : ?>
									<li>
										<div class="full-video">
											<?php echo get_post_meta( $post->ID, 'pyre_video', true ); ?>
										</div>
									</li>
								<?php endif; ?>
								<?php if ( has_post_thumbnail() && 'yes' != get_post_meta( $post->ID, 'pyre_show_first_featured_image', true ) ) : ?>
									<?php $attachment_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
									<?php $full_image       = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
									<?php $attachment_data  = wp_get_attachment_metadata( get_post_thumbnail_id() ); ?>
									<li>
										<a href="<?php echo $full_image[0]; ?>" data-rel="iLightbox[gallery<?php the_ID(); ?>]" title="<?php echo get_post_field( 'post_excerpt', get_post_thumbnail_id() ); ?>" data-title="<?php echo get_post_field( 'post_title', get_post_thumbnail_id() ); ?>" data-caption="<?php echo get_post_field( 'post_excerpt', get_post_thumbnail_id() ); ?>">
											<img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true ); ?>" role="presentation" />
										</a>
									</li>
								<?php endif; ?>
								<?php $i = 2; ?>
								<?php while ( $i <= Avada()->settings->get( 'posts_slideshow_number' ) ) : ?>
									<?php $attachment_new_id = kd_mfi_get_featured_image_id( 'featured-image-' . $i, 'page' ); ?>
									<?php if ( $attachment_new_id ) : ?>
										<?php $attachment_image = wp_get_attachment_image_src( $attachment_new_id, 'full' ); ?>
										<?php $full_image       = wp_get_attachment_image_src( $attachment_new_id, 'full' ); ?>
										<?php $attachment_data  = wp_get_attachment_metadata( $attachment_new_id ); ?>
										<li>
											<a href="<?php echo $full_image[0]; ?>" data-rel="iLightbox[gallery<?php the_ID(); ?>]" title="<?php echo get_post_field( 'post_excerpt', $attachment_new_id ); ?>" data-title="<?php echo get_post_field( 'post_title', $attachment_new_id ); ?>" data-caption="<?php echo get_post_field( 'post_excerpt', $attachment_new_id ); ?>">
												<img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo get_post_meta( $attachment_new_id, '_wp_attachment_image_alt', true ); ?>" role="presentation" />
											</a>
										</li>
									<?php endif; ?>
									<?php $i++; ?>
								<?php endwhile; ?>
							</ul>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; // Password check. ?>

			<div class="post-content">
				<?php the_content(); ?>

				<div class="authors-list">
				<?php
					// get all authors of blog
					$args = array(
						'role__not_in' => array('administrator','subscriber', 'customer')
					);
					$authors = get_users( $args );

					// loop and print cards
					foreach ($authors as $key => $author):
						// print_r($author);
						// Initialize needed variables.
						$author_id          = $author->ID;
						$author_name        = get_the_author_meta( 'display_name', $author_id );
						$author_avatar      = get_avatar( get_the_author_meta( 'email', $author_id ), '82' );
						$author_description = get_the_author_meta( 'description', $author_id );
						$author_custom      = get_the_author_meta( 'author_custom', $author_id );

						// If no description was added by user, add some default text and stats.
						if ( !empty( $author_description ) ) {
							$author_description .= '<br />';
						}
						$author_description .= sprintf( esc_html__( '%1$s has written %2$s articles.', 'Avada' ), $author_name, count_user_posts( $author_id ) );
				?>
					<div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2 fusion-one-half 1_2 <?php echo (($key+1) % 2 == 0)?'fusion-column-last':'fusion-column-first'; ?>">
						<div class="fusion-author">
							<div class="fusion-author-avatar">
								<a href="<?php echo get_author_posts_url( $author_id ); ?>">
									<?php echo $author_avatar; ?>
								</a>
							</div>
							<div class="fusion-author-info">
								<h3 class="fusion-author-title<?php echo ( Avada()->settings->get( 'disable_date_rich_snippet_pages' ) ) ? ' vcard' : ''; ?>">
									<a href="<?php echo get_author_posts_url( $author_id ); ?>">
									<?php printf(
										esc_html__( '%s', 'Avada' ),
										( Avada()->settings->get( 'disable_date_rich_snippet_pages' ) ) ? '<span class="fn">' . $author_name . '</span>' : $author_name
									); ?>
									</a>

									<?php // If user can edit his profile, offer a link for it. ?>
									<?php if ( current_user_can( 'edit_users' ) || get_current_user_id() == $author_id ) : ?>
										<span class="fusion-edit-profile">(<a href="<?php echo admin_url( 'profile.php?user_id=' . $author_id ); ?>"><?php _e( 'Edit profile', 'Avada' ); ?></a>)</span>
									<?php endif; ?>
								</h3>
								<?php echo $author_description; ?>
							</div>

							<div style="clear:both;"></div>

							<div class="fusion-author-social clearfix">
								<div class="fusion-author-tagline">
									<?php if ( $author_custom ) : ?>
										<?php echo $author_custom; ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
					
					<div class="clear"></div>
				</div>

				<?php avada_link_pages(); ?>
			</div>
			<?php if ( ! post_password_required( $post->ID ) ) : ?>
				<?php if ( class_exists( 'WooCommerce' ) ) : ?>
					<?php $woo_thanks_page_id = get_option( 'woocommerce_thanks_page_id' ); ?>
					<?php $is_woo_thanks_page = ( ! get_option( 'woocommerce_thanks_page_id' ) ) ? false : is_page( get_option( 'woocommerce_thanks_page_id' ) ); ?>
					<?php if ( Avada()->settings->get( 'comments_pages' ) && ! is_cart() && ! is_checkout() && ! is_account_page() && ! $is_woo_thanks_page ) : ?>
						<?php wp_reset_query(); ?>
						<?php comments_template(); ?>
					<?php endif; ?>
				<?php else : ?>
					<?php if ( Avada()->settings->get( 'comments_pages' ) ) : ?>
						<?php wp_reset_query(); ?>
						<?php comments_template(); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; // Password check. ?>
		</div>
	<?php endwhile; ?>
	<?php wp_reset_query(); ?>
</div>
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
