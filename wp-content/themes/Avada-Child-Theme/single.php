<?php
/**
 * Template used for single posts and other post-types
 * that don't have a specific template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>

<div id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
	<?php $post_pagination = get_post_meta( $post->ID, 'pyre_post_pagination', true ); ?>
	<?php if ( ( Avada()->settings->get( 'blog_pn_nav' ) && 'no' !== $post_pagination ) || ( ! Avada()->settings->get( 'blog_pn_nav' ) && 'yes' === $post_pagination ) ) : ?>
		<div class="single-navigation clearfix">
			<?php previous_post_link( '%link' ); ?>
			<?php next_post_link( '%link' ); ?>

			<div class="clearfix"></div>
		</div>
	<?php endif; ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>
			<?php $full_image = ''; ?>
			<?php if ( 'above' == Avada()->settings->get( 'blog_post_title' ) ) : ?>
				<?php echo wp_kses_post( avada_render_post_title( $post->ID, false, '', '2' ) ); ?>
				<?php the_subtitle('<h3 class="subtitle">', '</h3>'); ?>
			<?php elseif ( 'disabled' == Avada()->settings->get( 'blog_post_title' ) && Avada()->settings->get( 'disable_date_rich_snippet_pages' ) ) : ?>
				<span class="entry-title" style="display: none;"><?php the_title(); ?></span>
			<?php endif; ?>

			<?php if ( ( Avada()->settings->get( 'author_info' ) && 'no' != get_post_meta( $post->ID, 'pyre_author_info', true ) ) || ( ! Avada()->settings->get( 'author_info' ) && 'yes' == get_post_meta( $post->ID, 'pyre_author_info', true ) ) ) : ?>
				<div class="about-author fusion-author top-author">
					<div class="about-author-container">
						<div class="avatar">
							<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php echo get_avatar( get_the_author_meta( 'email' ), '72' ); ?></a>
						</div>
						<div class="fusion-author-info">
							
							<?php ob_start(); ?>
							<?php the_author_posts_link(); ?>
							<?php $title = sprintf( __( '%s', 'Avada' ), ob_get_clean() ); ?>
							<?php //echo Avada()->template->title_template( , '3' ); ?>
							<span class="fusion-author-title<?php echo ( Avada()->settings->get( 'disable_date_rich_snippet_pages' ) ) ? ' vcard' : ''; ?>">
								<?php printf(
									esc_html__( 'By %s', 'Avada' ),
									( Avada()->settings->get( 'disable_date_rich_snippet_pages' ) ) ? '<span class="fn">' . $title . '</span>' : $title
								); ?>

								<span>on <?php the_date(); ?></span>
							</span>
							
							<div class="description">
							<?php
								global $social_icons;

								$setting_name = 'social_sharing_box';

								if ( ( Avada()->settings->get( $setting_name ) && 'no' != get_post_meta( get_the_ID(), 'pyre_share_box', true ) ) || ( ! Avada()->settings->get( $setting_name ) && 'yes' == get_post_meta( get_the_ID(), 'pyre_share_box', true ) ) ) {

									$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
									$new_url = get_tiny_url(get_permalink( get_the_ID()));

									$sharingbox_social_icon_options = array(
										'sharingbox'        => 'yes',
										'icon_colors'       => Avada()->settings->get( 'sharing_social_links_icon_color' ),
										'box_colors'        => Avada()->settings->get( 'sharing_social_links_box_color' ),
										'icon_boxed'        => Avada()->settings->get( 'sharing_social_links_boxed' ),
										'icon_boxed_radius' => Fusion_Sanitize::size( Avada()->settings->get( 'sharing_social_links_boxed_radius' ) ),
										'tooltip_placement' => Avada()->settings->get( 'sharing_social_links_tooltip_placement' ),
										'linktarget'        => Avada()->settings->get( 'social_icons_new' ),
										'title'             => wp_strip_all_tags( get_the_title( get_the_ID() ), true ),
										'description'       => Avada()->blog->get_content_stripped_and_excerpted( 55, get_the_content() ),
										'link'              => $new_url,//wp_get_shortlink(get_the_ID()),//get_permalink( get_the_ID() ),
										'pinterest_image'   => ( $full_image ) ? $full_image[0] : '',
									);
									echo 'Share this: '.Avada()->social_sharing->render_social_icons( $sharingbox_social_icon_options );
								}
							?>
							</div>
						</div>
					</div>

					<div style="clear:both;"></div>

				</div>
			<?php endif; ?>

			<?php the_excerpt(); ?>

			<?php if ( ! post_password_required( $post->ID ) ) : ?>
				<?php if ( Avada()->settings->get( 'featured_images_single' ) ) : ?>
					<?php $video = get_post_meta( $post->ID, 'pyre_video', true ); ?>
					<?php if ( 0 < avada_number_of_featured_images() || $video ) : ?>
						<?php Avada()->images->set_grid_image_meta( array(
							'layout' => strtolower( 'large' ),
							'columns' => '1',
						) ); ?>
						<div class="fusion-flexslider flexslider fusion-flexslider-loading post-slideshow fusion-post-slideshow">
							<ul class="slides">
								<?php if ( $video ) : ?>
									<li>
										<div class="full-video">
											<?php // @codingStandardsIgnoreLine ?>
											<?php echo $video; ?>
										</div>
									</li>
								<?php endif; ?>
								<?php if ( has_post_thumbnail() && 'yes' != get_post_meta( $post->ID, 'pyre_show_first_featured_image', true ) ) : ?>
									<?php $attachment_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
									<?php $full_image       = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
									<?php $attachment_data  = wp_get_attachment_metadata( get_post_thumbnail_id() ); ?>
									<li>
										<?php if ( Avada()->settings->get( 'status_lightbox' ) && Avada()->settings->get( 'status_lightbox_single' ) ) : ?>
											<a href="<?php echo esc_url_raw( $full_image[0] ); ?>" data-rel="iLightbox[gallery<?php the_ID(); ?>]" title="<?php echo esc_attr( get_post_field( 'post_excerpt', get_post_thumbnail_id() ) ); ?>" data-title="<?php echo esc_attr( get_post_field( 'post_title', get_post_thumbnail_id() ) ); ?>" data-caption="<?php echo esc_attr( get_post_field( 'post_excerpt', get_post_thumbnail_id() ) ); ?>" aria-label="<?php echo esc_attr( get_post_field( 'post_title', get_post_thumbnail_id() ) ); ?>">
												<span class="screen-reader-text"><?php esc_attr_e( 'View Larger Image', 'Avada' ); ?></span>
												<?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
											</a>
										<?php else : ?>
											<?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
										<?php endif; ?>
									</li>
								<?php endif; ?>
								<?php $i = 2; ?>
								<?php while ( $i <= Avada()->settings->get( 'posts_slideshow_number' ) ) : ?>
									<?php $attachment_new_id = kd_mfi_get_featured_image_id( 'featured-image-' . $i, 'post' ); ?>
									<?php if ( $attachment_new_id ) : ?>
										<?php $attachment_image = wp_get_attachment_image_src( $attachment_new_id, 'full' ); ?>
										<?php $full_image       = wp_get_attachment_image_src( $attachment_new_id, 'full' ); ?>
										<?php $attachment_data  = wp_get_attachment_metadata( $attachment_new_id ); ?>
										<li>
											<?php if ( Avada()->settings->get( 'status_lightbox' ) && Avada()->settings->get( 'status_lightbox_single' ) ) : ?>
												<a href="<?php echo esc_url_raw( $full_image[0] ); ?>" data-rel="iLightbox[gallery<?php the_ID(); ?>]" title="<?php echo esc_attr( get_post_field( 'post_excerpt', $attachment_new_id ) ); ?>" data-title="<?php echo esc_attr( get_post_field( 'post_title', $attachment_new_id ) ); ?>" data-caption="<?php echo esc_attr( get_post_field( 'post_excerpt', $attachment_new_id ) ); ?>" aria-label="<?php echo esc_attr( get_post_field( 'post_title', get_post_thumbnail_id() ) ); ?>">
													<?php echo wp_get_attachment_image( $attachment_new_id, 'full' ); ?>
												</a>
											<?php else : ?>
												<?php echo wp_get_attachment_image( $attachment_new_id, 'full' ); ?>
											<?php endif; ?>
										</li>
									<?php endif; ?>
									<?php $i++; ?>
								<?php endwhile; ?>
							</ul>
						</div>
						<?php Avada()->images->set_grid_image_meta( array() ); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>

			<?php if ( 'below' == Avada()->settings->get( 'blog_post_title' ) ) : ?>
				<?php echo wp_kses_post( avada_render_post_title( $post->ID, false, '', '2' ) ); ?>
				<?php the_subtitle('<h3 class="subtitle">', '</h3>'); ?>
			<?php endif; ?>
			<div class="post-content">
				<?php the_content(); ?>
				<?php fusion_link_pages(); ?>
			</div>

			<?php if ( ! post_password_required( $post->ID ) ) : ?>
				<?php echo avada_render_post_metadata( 'single' ); ?>
				
				<?php
					global $social_icons;

					$setting_name = 'social_sharing_box';

					if ( ( Avada()->settings->get( $setting_name ) && 'no' != get_post_meta( get_the_ID(), 'pyre_share_box', true ) ) || ( ! Avada()->settings->get( $setting_name ) && 'yes' == get_post_meta( get_the_ID(), 'pyre_share_box', true ) ) ) {

						$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );

						$sharingbox_soical_icon_options = array(
							'sharingbox'        => 'yes',
							'icon_colors'       => Avada()->settings->get( 'sharing_social_links_icon_color' ),
							'box_colors'        => Avada()->settings->get( 'sharing_social_links_box_color' ),
							'icon_boxed'        => Avada()->settings->get( 'sharing_social_links_boxed' ),
							'icon_boxed_radius' => Fusion_Sanitize::size( Avada()->settings->get( 'sharing_social_links_boxed_radius' ) ),
							'tooltip_placement' => Avada()->settings->get( 'sharing_social_links_tooltip_placement' ),
							'linktarget'        => Avada()->settings->get( 'social_icons_new' ),
							'title'             => wp_strip_all_tags( get_the_title( get_the_ID() ), true ),
							'description'       => Avada()->blog->get_content_stripped_and_excerpted( 55, get_the_content() ),
							'link'              => $new_url,//get_permalink( get_the_ID() ),
							'pinterest_image'   => ( $full_image ) ? $full_image[0] : '',
						);
						?>
						<div class="fusion-sharing-box fusion-single-sharing-box share-box">
							<h4><?php echo apply_filters( 'fusion_sharing_box_tagline', Avada()->settings->get( 'sharing_social_tagline' ) ); ?></h4>
							<?php echo Avada()->social_sharing->render_social_icons( $sharingbox_soical_icon_options ); ?>
						</div>
						<?php
					}
				?>

				<?php if ( ( Avada()->settings->get( 'author_info' ) && 'no' != get_post_meta( $post->ID, 'pyre_author_info', true ) ) || ( ! Avada()->settings->get( 'author_info' ) && 'yes' == get_post_meta( $post->ID, 'pyre_author_info', true ) ) ) : ?>
					<div class="about-author fusion-author">
						<div class="about-author-container">
							<div class="avatar">
								<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php echo get_avatar( get_the_author_meta( 'email' ), '72' ); ?></a>
							</div>
							<div class="fusion-author-info">
								
								<?php ob_start(); ?>
								<?php the_author_posts_link(); ?>
								<?php $title = sprintf( __( '%s', 'Avada' ), ob_get_clean() ); ?>
								<?php //echo Avada()->template->title_template( , '3' ); ?>
								<h3 class="fusion-author-title<?php echo ( Avada()->settings->get( 'disable_date_rich_snippet_pages' ) ) ? ' vcard' : ''; ?>">
									<?php printf(
										esc_html__( 'About %s', 'Avada' ),
										( Avada()->settings->get( 'disable_date_rich_snippet_pages' ) ) ? '<span class="fn">' . $title . '</span>' : $title
									); ?>
								</h3>
								
								<div class="description">
									<?php the_author_meta( 'description' ); ?>
								</div>
							</div>
						</div>

						<div style="clear:both;"></div>

						<?php $author_custom = get_the_author_meta( 'author_custom', get_the_author_meta( 'ID' ) ); ?>
						<div class="fusion-author-social clearfix">
							<div class="fusion-author-tagline">
								<?php if ( $author_custom ) : ?>
									<?php echo $author_custom; ?>
								<?php endif; ?>
							</div>

							<?php

							// Get the social icons for the author set on his profile page.
							$author_soical_icon_options = array(
								'authorpage'        => 'yes',
								'author_id'         => get_the_author_meta( 'ID' ),
								'position'          => 'author',
								'icon_colors'       => Avada()->settings->get( 'social_links_icon_color' ),
								'box_colors'        => Avada()->settings->get( 'social_links_box_color' ),
								'icon_boxed'        => Avada()->settings->get( 'social_links_boxed' ),
								'icon_boxed_radius' => Fusion_Sanitize::size( Avada()->settings->get( 'social_links_boxed_radius' ) ),
								'tooltip_placement' => Avada()->settings->get( 'social_links_tooltip_placement' ),
								'linktarget'        => Avada()->settings->get( 'social_icons_new' ),
							);

							echo Avada()->social_sharing->render_social_icons( $author_soical_icon_options );

							?>
						</div>
					</div>
				<?php endif; ?>
				<?php avada_render_related_posts( get_post_type() ); // Render Related Posts. ?>

				<?php $post_comments = get_post_meta( $post->ID, 'pyre_post_comments', true ); ?>
				<?php if ( ( Avada()->settings->get( 'blog_comments' ) && 'no' !== $post_comments ) || ( ! Avada()->settings->get( 'blog_comments' ) && 'yes' === $post_comments ) ) : ?>
					<?php wp_reset_postdata(); ?>
					<?php comments_template(); ?>
				<?php endif; ?>
			<?php endif; ?>
		</article>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
</div>
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
